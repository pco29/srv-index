# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

print "opa"
#print V.count
#print Person.count


files = Dir["cotv/json/*"]
for name in files
    #print(name+"\n")
    data = JSON.load(File.new(name))

    transcription = ""
    trans_file = "cotv/textos2/"+data["id"].to_s
    if File.exist?(trans_file)
      file = File.open(trans_file)
      transcription = file.read
      file.close
    end

    video = Video.new(title: data["title"], youtube_id: data["id"], transcription: transcription)
    video.save()
    print(data["id"], data["title"], "\n")
end
