class CreateVideos < ActiveRecord::Migration[7.0]
  def change
    create_table :videos do |t|
      t.string :title
      t.date :published_at
      t.integer :duration
      t.string :youtube_id
      t.text :transcription

      t.timestamps
    end
  end
end
