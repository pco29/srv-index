#require 'json'

desc 'Say hello!'
task :cadastra => :environment do

    #data = MultiJson.load(json, options)

    raw = File.open("dados.json").read()
    json = JSON(raw)

    json.each do |video|
        puts(video)
        yt_id = video["URL"].split("=")[1]
        Video.create(title: video["title"], youtube_id: yt_id, duration: video["duration"], published_at: video["date"])
    end
    #Video.create(name: "teste", title: "teste", youtube_id: "123")
end
