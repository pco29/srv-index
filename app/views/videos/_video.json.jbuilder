json.extract! video, :id, :name, :title, :youtube_id, :transcription, :created_at, :updated_at
json.url video_url(video, format: :json)
