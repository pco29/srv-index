class ApplicationController < ActionController::Base
  def index
  end

  def search
    #Video.__elasticsearch__.create_index!
    s_search = params[:search]
    #@objects = Video.search(s_search).records
    @objects = Elasticsearch::Model.search(s_search, [], size: 100)
  end
end
